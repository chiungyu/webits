package utils

import (
	"context"
	"fmt"
	"net/http"

	"github.com/rs/xid"
	"go.uber.org/zap"
)

type requestIDKeyType int

const requestIDKey requestIDKeyType = 1

// GetRequestID returns request ID in request context
func GetRequestID(r *http.Request) string {
	requestID, _ := r.Context().Value(requestIDKey).(string)
	return requestID
}

type logResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

func newLogResponseWriter(w http.ResponseWriter) *logResponseWriter {
	return &logResponseWriter{w, http.StatusOK}
}

func (lrw *logResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}

func logResponse(lrw *logResponseWriter, r *http.Request) {
	defer zap.L().Sync()
	zap.L().Info(fmt.Sprintf("%d %s %s", lrw.statusCode, r.Method, r.URL.Path),
		zap.String("addr", r.RemoteAddr),
		zap.String("x-forwarded-for", r.Header.Get("x-forwarded-for")),
		zap.String("rid", GetRequestID(r)))
}

// LoggingMiddleware adds request ids and logs responses.
func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r = r.WithContext(context.WithValue(r.Context(), requestIDKey, xid.New().String()))

		lrw := newLogResponseWriter(w)
		defer logResponse(lrw, r)

		next.ServeHTTP(lrw, r)
	})
}
