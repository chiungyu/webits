package utils

import (
	"log"

	"go.uber.org/zap"
)

// SetLogger replaces glabal logger and redirects STD logger.
func SetLogger(options *Options) {
	var logger *zap.Logger
	var err error
	if options.DevMode {
		logger, err = zap.NewDevelopment()
	} else {
		logger, err = zap.NewProduction()
	}
	if err != nil {
		log.Fatalln("failed to initialize logger", err)
	}
	zap.ReplaceGlobals(logger)
	zap.RedirectStdLog(logger)
	defer logger.Sync()
	logger.Info("logger initialized")
}
