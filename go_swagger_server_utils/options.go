package utils

// Options for the implementation
type Options struct {
	DevMode bool   `short:"d" long:"dev-mode" description:"sets formatted logger for development mode"`
	UIDir   string `short:"u" long:"ui-dir"  description:"serves ui files under this directory"`
}
