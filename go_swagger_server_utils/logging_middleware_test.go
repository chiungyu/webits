package utils

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"go.uber.org/zap/zaptest/observer"
)

type testLoggingHandler struct {
	requestID string
	code      int
}

func (h *testLoggingHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.requestID = GetRequestID(r)
	if h.code != http.StatusOK {
		w.WriteHeader(h.code)
	}
}

func TestLoggingMiddlewares(t *testing.T) {
	{ // Basic usage with 200 OK
		core, recorded := observer.New(zapcore.InfoLevel)
		zap.ReplaceGlobals(zap.New(core))
		th := &testLoggingHandler{code: http.StatusOK}
		h := LoggingMiddleware(th)

		r := httptest.NewRequest("GET", "/test", nil)
		r.RemoteAddr = "test-addr"
		r.Header.Add("x-forwarded-for", "test-x-forwarded-for")
		h.ServeHTTP(httptest.NewRecorder(), r)

		logged := recorded.All()
		assert.Len(t, logged, 1)
		assert.Equal(t, "200 GET /test", logged[0].Entry.Message)

		m := logged[0].ContextMap()
		assert.Len(t, m, 3)
		assert.Equal(t, "test-addr", m["addr"])
		assert.Equal(t, "test-x-forwarded-for", m["x-forwarded-for"])
		assert.Equal(t, th.requestID, m["rid"])
	}

	{ // Basic usage with 500 InternalServerError
		core, recorded := observer.New(zapcore.InfoLevel)
		zap.ReplaceGlobals(zap.New(core))
		th := &testLoggingHandler{code: http.StatusInternalServerError}
		h := LoggingMiddleware(th)

		r := httptest.NewRequest("GET", "/test", nil)
		r.RemoteAddr = "test-addr"
		r.Header.Add("x-forwarded-for", "test-x-forwarded-for")
		h.ServeHTTP(httptest.NewRecorder(), r)

		logged := recorded.All()
		assert.Len(t, logged, 1)
		assert.Equal(t, "500 GET /test", logged[0].Entry.Message)

		m := logged[0].ContextMap()
		assert.Len(t, m, 3)
		assert.Equal(t, "test-addr", m["addr"])
		assert.Equal(t, "test-x-forwarded-for", m["x-forwarded-for"])
		assert.Equal(t, th.requestID, m["rid"])
	}
}
