package user

import (
	"encoding/json"
	"io/ioutil"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestTokenAuth(t *testing.T) {
	{
		assert.Implements(t, (*Auth)(nil), new(TokenAuth))
	}
	{ // Set
		ta := NewTokenAuth("Token ")
		rr := httptest.NewRecorder()
		ta.Set(rr, "testID", "testToken", time.Unix(0, 1<<42))

		rr.Flush()
		result := rr.Result()
		assert.Equal(t, "application/json", result.Header.Get("Content-Type"))

		var signIn signInResponse
		dec := json.NewDecoder(result.Body)
		err := dec.Decode(&signIn)
		assert.NoError(t, err)
		assert.Equal(t, "testToken", signIn.Token)
		assert.Equal(t, int64(1<<42), signIn.Expiry)
	}
	{ // No-op Clear
		ta := NewTokenAuth("Token ")
		rr := httptest.NewRecorder()
		ta.Clear(rr)

		result := rr.Result()
		b, err := ioutil.ReadAll(result.Body)
		assert.NoError(t, err)
		assert.Empty(t, b)

	}
	{
		ta := NewTokenAuth("Token ")

		// Request without sign-in cookie
		req := httptest.NewRequest("GET", "/", nil)
		{
			id, token, err := ta.Get(req)
			assert.NoError(t, err)
			assert.Empty(t, id)
			assert.Empty(t, token)
		}

		req.Header.Set("Authorization", "Token testToken")
		{
			id, token, err := ta.Get(req)
			assert.NoError(t, err)
			assert.Empty(t, id)
			assert.Equal(t, "testToken", token)
		}

		// Bad prefix
		req.Header.Set("Authorization", "Bearer testToken")
		{
			id, token, err := ta.Get(req)
			assert.EqualError(t, err, "unexpected Authorization header value: Bearer testToken")
			assert.Empty(t, id)
			assert.Empty(t, token)
		}

		// No prefix
		req.Header.Set("Authorization", "testToken")
		{
			id, token, err := ta.Get(req)
			assert.EqualError(t, err, "unexpected Authorization header value: testToken")
			assert.Empty(t, id)
			assert.Empty(t, token)
		}

		// Unexpected basic Auth
		req.SetBasicAuth("testUser", "testPassword")
		{
			id, token, err := ta.Get(req)
			assert.EqualError(t, err, "unexpected Authorization header value: Basic dGVzdFVzZXI6dGVzdFBhc3N3b3Jk")
			assert.Empty(t, id)
			assert.Empty(t, token)
		}

	}
}
