package user

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/securecookie"
)

// CookieAuth for user sign-in
type CookieAuth struct {
	name     string
	encoder  securecookie.Codec
	decoders []securecookie.Codec
}

type signIn struct{ ID, Token string }

// NewCookieAuth returns a new user sign-in cookie codec with the provided
// cookie name, codec, and previously used codecs (for decoding older cookies).
func NewCookieAuth(name string, codecs ...securecookie.Codec) (*CookieAuth, error) {
	if len(codecs) == 0 {
		return nil, fmt.Errorf("missing cookie codec")
	}
	return &CookieAuth{
		encoder:  codecs[0],
		decoders: codecs,
		name:     name,
	}, nil
}

// Make makes sign-in cookie.
func (c *CookieAuth) Make(id, token string) (string, error) {
	return c.encoder.Encode(c.name, &signIn{ID: id, Token: token})
}

// Set sets sign-in cookie.
func (c *CookieAuth) Set(w http.ResponseWriter, id, token string, expiry time.Time) error {
	value, err := c.Make(id, token)
	if err != nil {
		return err
	}

	http.SetCookie(w, &http.Cookie{
		Name:     c.name,
		Value:    value,
		Path:     "/",
		MaxAge:   int((expiry.UnixNano() - time.Now().UnixNano()) / 1000000000),
		Secure:   true,
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	})
	return nil
}

// Clear clears sign-in cookie.
func (c *CookieAuth) Clear(w http.ResponseWriter) {
	http.SetCookie(w, &http.Cookie{
		Name:   c.name,
		Value:  "",
		MaxAge: -1,
	})
}

// Get gets sign-in cookie from request.
func (c *CookieAuth) Get(r *http.Request) (string, string, error) {
	cookie, err := r.Cookie(c.name)
	if err != nil { // http.ErrNoCookie
		return "", "", nil
	}
	var value signIn
	err = securecookie.DecodeMulti(c.name, cookie.Value, &value, c.decoders...)
	if err != nil {

		return "", "", fmt.Errorf("fail to parse sign-in cookie [%s]: %w", cookie.Value, err)
	}
	return value.ID, value.Token, nil
}
