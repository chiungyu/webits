package user

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gorilla/securecookie"
	"github.com/stretchr/testify/assert"
)

func TestCookieAuth(t *testing.T) {
	{
		assert.Implements(t, (*Auth)(nil), new(CookieAuth))
	}
	{
		_, err := NewCookieAuth("sign-in")
		assert.EqualError(t, err, "missing cookie codec")
	}
	{ // Missing hash key
		c, err := NewCookieAuth("sign-in", &securecookie.SecureCookie{ /* no hash key */ })
		assert.NoError(t, err)
		assert.Error(t, c.Set(httptest.NewRecorder(), "test-user", "test-token", time.Now().Add(time.Hour)))
	}
	{
		c, err := NewCookieAuth("sign-in", securecookie.New(bytes.Repeat([]byte{'0'}, 64), bytes.Repeat([]byte{'0'}, 32)))
		assert.NoError(t, err)
		rr := httptest.NewRecorder()
		c.Clear(rr)

		rr.Flush()
		cookies := rr.Result().Cookies()
		assert.Len(t, cookies, 1)
		clearCookie := cookies[0]
		assert.Equal(t, "sign-in", clearCookie.Name)
		assert.Equal(t, "", clearCookie.Value)
		assert.Equal(t, -1, clearCookie.MaxAge)
	}
	{
		codecs := []securecookie.Codec{
			securecookie.New(bytes.Repeat([]byte{'0'}, 64), bytes.Repeat([]byte{'0'}, 32)),
			securecookie.New(bytes.Repeat([]byte{'1'}, 64), bytes.Repeat([]byte{'1'}, 32)),
			securecookie.New(bytes.Repeat([]byte{'2'}, 64), bytes.Repeat([]byte{'2'}, 32)),
			securecookie.New(bytes.Repeat([]byte{'3'}, 64), bytes.Repeat([]byte{'3'}, 32)),
			securecookie.New(bytes.Repeat([]byte{'4'}, 64), bytes.Repeat([]byte{'4'}, 32)),
		}

		// Use codec[3] to encode a sign-in cookie
		c, err := NewCookieAuth("sign-in", codecs[3])
		assert.NoError(t, err)
		rr := httptest.NewRecorder()
		assert.NoError(t, c.Set(rr, "test-user", "test-token", time.Now().Add(time.Hour)))

		rr.Flush()
		cookies := rr.Result().Cookies()
		assert.Len(t, cookies, 1)
		cookieFromCodec3 := cookies[0]
		assert.Equal(t, "sign-in", cookieFromCodec3.Name)
		assert.True(t, cookieFromCodec3.Secure)
		assert.True(t, cookieFromCodec3.HttpOnly)
		assert.Equal(t, http.SameSiteStrictMode, cookieFromCodec3.SameSite)

		// Request without sign-in cookie
		req := httptest.NewRequest("GET", "/", nil)
		{
			id, token, err := c.Get(req)
			assert.NoError(t, err)
			assert.Empty(t, id)
			assert.Empty(t, token)
		}

		// Request with the cookie from above.
		req.AddCookie(cookieFromCodec3)
		{
			id, token, err := c.Get(req)
			assert.NoError(t, err)
			assert.Equal(t, "test-user", id)
			assert.Equal(t, "test-token", token)
		}

		{ // codec[3] provided for decoding
			c0t4, err := NewCookieAuth("sign-in", codecs...)
			assert.NoError(t, err)
			id, token, err := c0t4.Get(req)
			assert.NoError(t, err)
			assert.Equal(t, "test-user", id)
			assert.Equal(t, "test-token", token)
		}

		{ // codec[3] not provided
			c0t2, err := NewCookieAuth("sign-in", codecs[:2]...)
			assert.NoError(t, err)
			_, _, err = c0t2.Get(req)
			assert.Error(t, err)
		}

	}
}
