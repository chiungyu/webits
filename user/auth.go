package user

import (
	"net/http"
	"time"
)

// Auth interface for setting, getting, and clearing auth tokens
type Auth interface {
	Set(w http.ResponseWriter, id, token string, expiry time.Time) error
	Get(r *http.Request) (string, string, error) // id, token, err
	Clear(w http.ResponseWriter)
}
