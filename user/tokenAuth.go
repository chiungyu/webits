package user

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"
)

// TokenAuth for user sign-in
type TokenAuth struct {
	prefix string
}

type signInResponse struct {
	Token  string
	Expiry int64
}

// NewTokenAuth returns a new user sign-in TokenAuth object with the provided token prefix (e.g., "Bearer ").
func NewTokenAuth(prefix string) *TokenAuth {
	return &TokenAuth{prefix: prefix}
}

// Set returns token and expiry in json response.
func (t *TokenAuth) Set(w http.ResponseWriter, id, token string, expiry time.Time) error {
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(signInResponse{Token: token, Expiry: expiry.UnixNano()})
}

// Clear does nothing for TokenAuth. Clients are responsible for cleaning up invalid tokens.
func (t *TokenAuth) Clear(w http.ResponseWriter) {
	return
}

// Get gets sign-in token from request.
func (t *TokenAuth) Get(r *http.Request) (string, string, error) {
	a := r.Header.Get("Authorization")
	if len(a) == 0 {
		return "", "", nil
	}
	if !strings.HasPrefix(a, t.prefix) {
		return "", "", fmt.Errorf("unexpected Authorization header value: %s", a)
	}
	return "" /* token-only, no id */, a[len(t.prefix):], nil
}
