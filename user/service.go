package user

import "time"

// SignIn data for a user
type SignIn struct {
	ID     string
	Token  string
	Expiry time.Time
	Data   interface{} // E.g., roles, permissions, ...
}

// ServiceError type
type ServiceError int

// SignInError values
const (
	ServiceBackendError ServiceError = iota
	ServiceBadAuth
	ServiceExpiredAuth
	ServiceMissingPermission
)

// Error implementation
func (err ServiceError) Error() string {
	switch err {
	case ServiceBackendError:
		return "user service backend error"
	case ServiceBadAuth:
		return "user auth is not valid"
	case ServiceExpiredAuth:
		return "user auth expired"
	case ServiceMissingPermission:
		return "user missing required permission"
	default:
		return "unknown user service error"
	}
}

// Service defines the backend service for user auth.
type Service interface {
	SignIn(id, password, aux interface{}) (*SignIn, error)
	CheckSignIn(id, token, aux interface{}) (*SignIn, error)
	SignOut(id, token, aux interface{}) error
}
