package middleware

import (
	"fmt"
	"net/http"

	"github.com/rs/xid"
	"gitlab.com/chiungyu/webits/context"
	"gitlab.com/chiungyu/webits/user"
	"go.uber.org/zap"
)

// ContextMiddleware injects tracking id, sign-in, and renderer into request context.
func ContextMiddleware(userService user.Service, userAuth user.Auth, logger *zap.Logger) (func(http.Handler) http.Handler, error) {
	if userService == nil || userAuth == nil || logger == nil {
		return nil, fmt.Errorf("nil argument(s)")
	}
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			requestID := xid.New().String()

			userID, userToken, err := userAuth.Get(r)
			if err != nil {
				defer logger.Sync()
				logger.Error("error parsing user sign-in cookie", zap.Error(err))

				// Clear the broken cookie and reject the request.
				userAuth.Clear(w)
				http.Error(w, "Unauthorized", http.StatusUnauthorized)
				return
			}

			if userID == "" && userToken == "" {
				r = context.New(requestID, "", "", nil).Set(r)
			} else {
				signIn, err := userService.CheckSignIn(userID, userToken, nil)
				if signIn == nil || err != nil {
					defer logger.Sync()

					if err == user.ServiceBackendError {
						logger.Error("failed to check sign-in token",
							zap.String("rID", requestID),
							zap.String("uID", userID),
							zap.Error(err))

						http.Error(w, "Internal server error", http.StatusInternalServerError)
					} else {
						userAuth.Clear(w)

						logger.Warn("invalid sign-in token",
							zap.String("rID", requestID),
							zap.String("uID", userID))

						http.Error(w, "Unauthorized", http.StatusUnauthorized)
					}
					return
				}
				r = context.New(requestID, signIn.ID, signIn.Token, signIn.Data).Set(r)
			}

			next.ServeHTTP(w, r)
		})
	}, nil
}
