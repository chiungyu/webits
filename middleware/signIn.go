package middleware

import (
	"fmt"
	"net/http"

	"gitlab.com/chiungyu/webits/context"

	"go.uber.org/zap"
)

// SignInMiddleware returns StatusUnauthorized for requests without valid sign-in.
func SignInMiddleware(logger *zap.Logger) (func(http.Handler) http.Handler, error) {
	if logger == nil {
		return nil, fmt.Errorf("nil logger")
	}
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := context.Get(r)
			if ctx == nil {
				defer logger.Sync()
				logger.Error("context not filled before sign-in middleware is called")
				http.Error(w, "Internal server error", /* we don't have a language-specific printer */
					http.StatusInternalServerError)
				return
			}

			if ctx.UserID() == "" {
				http.Error(w, "Unauthorized", http.StatusUnauthorized)
				return
			}

			next.ServeHTTP(w, r)
		})
	}, nil
}
