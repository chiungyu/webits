package middleware

import (
	"fmt"
	"net/http"

	"gitlab.com/chiungyu/webits/context"

	"go.uber.org/zap"
)

// LoggingMiddleware logs requests and responses.
func LoggingMiddleware(logger *zap.Logger) (func(http.Handler) http.Handler, error) {
	if logger == nil {
		return nil, fmt.Errorf("nil logger")
	}
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			lrw := newLogResponseWriter(w)
			defer logResponse(lrw, r, logger)

			logRequest(r, logger)

			next.ServeHTTP(lrw, r)
		})
	}, nil
}

func logRequest(r *http.Request, logger *zap.Logger) {
	defer logger.Sync()
	ctx := context.Get(r)
	if ctx == nil {
		logger.Info(fmt.Sprintf("%s %s", r.Method, r.URL.Path),
			zap.String("addr", r.RemoteAddr),
			zap.String("x-forwarded-for", r.Header.Get("x-forwarded-for")))
		return
	}
	logger.Info(fmt.Sprintf("%s %s", r.Method, r.URL.Path),
		zap.String("addr", r.RemoteAddr),
		zap.String("x-forwarded-for", r.Header.Get("x-forwarded-for")),
		zap.String("rID", ctx.RequestID()),
		zap.String("uID", ctx.UserID()))
}

type logResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

func newLogResponseWriter(w http.ResponseWriter) *logResponseWriter {
	return &logResponseWriter{w, http.StatusOK}
}

func (lrw *logResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}

func logResponse(lrw *logResponseWriter, r *http.Request, logger *zap.Logger) {
	defer logger.Sync()
	ctx := context.Get(r)
	if ctx == nil {
		logger.Info(fmt.Sprintf("%d %s %s", lrw.statusCode, r.Method, r.URL.Path))
		return
	}
	logger.Info(fmt.Sprintf("%d %s %s", lrw.statusCode, r.Method, r.URL.Path),
		zap.String("rid", ctx.RequestID()))
}

// ResponseLoggingMiddleware logs responses without request and context, for e.g.,
// static file & redirect handlers.
func ResponseLoggingMiddleware(logger *zap.Logger) (func(http.Handler) http.Handler, error) {
	if logger == nil {
		return nil, fmt.Errorf("nil logger")
	}
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			lrw := newLogResponseWriter(w)
			defer logResponseNoContext(lrw, r, logger)

			next.ServeHTTP(lrw, r)
		})
	}, nil
}

func logResponseNoContext(lrw *logResponseWriter, r *http.Request, logger *zap.Logger) {
	logger.Info(fmt.Sprintf("%d %s %s", lrw.statusCode, r.Method, r.URL.Path))
}
