package middleware

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/chiungyu/webits/context"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"go.uber.org/zap/zaptest/observer"
)

type logHelloHandler struct {
	logger *zap.Logger
}

func (lhh *logHelloHandler) ServeHTTP(http.ResponseWriter, *http.Request) {
	lhh.logger.Info("Hello")
}

func TestLoggingMiddlewares(t *testing.T) {
	{ // Basic usage
		core, recorded := observer.New(zapcore.InfoLevel)
		logger := zap.New(core)
		lm, err := LoggingMiddleware(logger)
		assert.NoError(t, err)
		h := lm(&logHelloHandler{logger: logger})

		r := httptest.NewRequest("GET", "/test", nil)
		r.RemoteAddr = "test-addr"
		r.Header.Add("x-forwarded-for", "test-x-forwarded-for")
		h.ServeHTTP(httptest.NewRecorder(), r)

		logged := recorded.All()
		assert.Len(t, logged, 3)
		assert.Equal(t, "GET /test", logged[0].Entry.Message)
		assert.Equal(t, "Hello", logged[1].Entry.Message)
		assert.Equal(t, "200 GET /test", logged[2].Entry.Message)

		m0 := logged[0].ContextMap()
		assert.Equal(t, "test-addr", m0["addr"])
		assert.Equal(t, "test-x-forwarded-for", m0["x-forwarded-for"])
	}
	{ // With context
		core, recorded := observer.New(zapcore.InfoLevel)
		logger := zap.New(core)
		lm, err := LoggingMiddleware(logger)
		assert.NoError(t, err)
		h := lm(&logHelloHandler{logger: logger})

		r := httptest.NewRequest("GET", "/test", nil)
		r.RemoteAddr = "test-addr"
		r.Header.Add("x-forwarded-for", "test-x-forwarded-for")
		r = context.New("testRequestID", "testUser", "testToken", nil).Set(r)

		h.ServeHTTP(httptest.NewRecorder(), r)

		logged := recorded.All()
		assert.Len(t, logged, 3)
		assert.Equal(t, "GET /test", logged[0].Entry.Message)
		assert.Equal(t, "Hello", logged[1].Entry.Message)
		assert.Equal(t, "200 GET /test", logged[2].Entry.Message)

		m0 := logged[0].ContextMap()
		assert.Equal(t, "test-addr", m0["addr"])
		assert.Equal(t, "test-x-forwarded-for", m0["x-forwarded-for"])
		assert.Equal(t, "testRequestID", m0["rID"])
		assert.Equal(t, "testUser", m0["uID"])

	}

	{ // 404
		core, recorded := observer.New(zapcore.InfoLevel)
		logger := zap.New(core)
		lm, err := LoggingMiddleware(logger)
		assert.NoError(t, err)
		h := lm(http.NotFoundHandler())

		r := httptest.NewRequest("GET", "/test", nil)
		r.RemoteAddr = "test-addr"
		r.Header.Add("x-forwarded-for", "test-x-forwarded-for")

		h.ServeHTTP(httptest.NewRecorder(), r)

		logged := recorded.All()
		assert.Len(t, logged, 2)
		assert.Equal(t, "GET /test", logged[0].Entry.Message)
		assert.Equal(t, "404 GET /test", logged[1].Entry.Message)

		m0 := logged[0].ContextMap()
		assert.Equal(t, "test-addr", m0["addr"])
		assert.Equal(t, "test-x-forwarded-for", m0["x-forwarded-for"])
	}
	{ // nil logger
		_, err := LoggingMiddleware(nil)
		assert.EqualError(t, err, "nil logger")
	}

	{ // Response only
		core, recorded := observer.New(zapcore.InfoLevel)
		logger := zap.New(core)
		lm, err := ResponseLoggingMiddleware(logger)
		assert.NoError(t, err)
		h := lm(&logHelloHandler{logger: logger})

		r := httptest.NewRequest("GET", "/test", nil)
		h.ServeHTTP(httptest.NewRecorder(), r)

		logged := recorded.All()
		assert.Len(t, logged, 2)
		assert.Equal(t, "Hello", logged[0].Entry.Message)
		assert.Equal(t, "200 GET /test", logged[1].Entry.Message)
	}
	{ // nil logger
		_, err := ResponseLoggingMiddleware(nil)
		assert.EqualError(t, err, "nil logger")
	}
}
