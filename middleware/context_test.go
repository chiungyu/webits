package middleware

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/chiungyu/webits/context"
	"gitlab.com/chiungyu/webits/user"

	"github.com/gorilla/securecookie"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"go.uber.org/zap/zaptest/observer"
)

type getContextHandler struct {
	ctx *context.Context
}

func (gch *getContextHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	gch.ctx = context.Get(r)
}

type mockUserService map[string]struct {
	signIn *user.SignIn
	err    error
}

// SignIn implementation -- not used by this test
func (s mockUserService) SignIn(id, password, aux interface{}) (*user.SignIn, error) {
	return nil, user.ServiceBackendError
}

// CheckSignIn implementation
func (s mockUserService) CheckSignIn(id, token, aux interface{}) (*user.SignIn, error) {
	if r, ok := s[fmt.Sprintf("%s|%s", id, token)]; ok {
		return r.signIn, r.err
	}
	return nil, user.ServiceBackendError
}

// SignOut implementation -- not used by this test
func (s mockUserService) SignOut(id, token, aux interface{}) error {
	return user.ServiceBackendError
}

func TestContextMiddleware(t *testing.T) {
	{ // Base usage
		userService := mockUserService{
			"testUser|testToken": {
				signIn: &user.SignIn{
					ID:     "checkedUser",
					Token:  "checkedToken",
					Expiry: time.Now().Add(time.Hour),
					Data:   "testData",
				},
				err: nil,
			},
		}

		userCookieAuth, err := user.NewCookieAuth("sign-in", securecookie.New(bytes.Repeat([]byte{'0'}, 64), bytes.Repeat([]byte{'0'}, 32)))
		assert.NoError(t, err)

		core, recorded := observer.New(zapcore.ErrorLevel)
		logger := zap.New(core)

		gch := &getContextHandler{}
		cm, err := ContextMiddleware(userService, userCookieAuth, logger)
		assert.NoError(t, err)
		h := cm(gch)

		r := httptest.NewRequest("GET", "/test", nil)
		userCookie, err := userCookieAuth.Make("testUser", "testToken")
		assert.NoError(t, err)
		r.AddCookie(&http.Cookie{Name: "sign-in", Value: userCookie})
		h.ServeHTTP(httptest.NewRecorder(), r)

		assert.Equal(t, 0, recorded.Len()) // No error

		ctx := gch.ctx
		assert.NotNil(t, ctx)
		assert.NotEmpty(t, ctx.RequestID())

		// User info from cookie
		assert.Equal(t, "checkedUser", ctx.UserID())
		assert.Equal(t, "checkedToken", ctx.UserToken())
		assert.Equal(t, "testData", ctx.UserData())
	}
	{ // Base usage without sign-in cookie
		userService := mockUserService{}
		userCookieAuth, err := user.NewCookieAuth("sign-in", securecookie.New(bytes.Repeat([]byte{'0'}, 64), bytes.Repeat([]byte{'0'}, 32)))
		assert.NoError(t, err)

		core, recorded := observer.New(zapcore.ErrorLevel)
		logger := zap.New(core)

		gch := &getContextHandler{}
		cm, err := ContextMiddleware(userService, userCookieAuth, logger)
		assert.NoError(t, err)
		h := cm(gch)

		r := httptest.NewRequest("GET", "/test", nil)
		h.ServeHTTP(httptest.NewRecorder(), r)

		assert.Equal(t, 0, recorded.Len()) // No error

		ctx := gch.ctx
		assert.NotNil(t, ctx)
		assert.NotEmpty(t, ctx.RequestID())

		// No user cookie
		assert.Empty(t, ctx.UserID())
		assert.Empty(t, ctx.UserToken())
	}

	{ // Broken user cookie
		userService := mockUserService{}
		userCookieAuth, err := user.NewCookieAuth("sign-in", securecookie.New(bytes.Repeat([]byte{'0'}, 64), bytes.Repeat([]byte{'0'}, 32)))
		assert.NoError(t, err)

		core, recorded := observer.New(zapcore.ErrorLevel)
		logger := zap.New(core)

		gch := &getContextHandler{}
		cm, err := ContextMiddleware(userService, userCookieAuth, logger)
		assert.NoError(t, err)
		h := cm(gch)

		r := httptest.NewRequest("GET", "/test", nil)
		r.AddCookie(&http.Cookie{Name: "sign-in", Value: "brokenUserCookie"})
		responseRecorder := httptest.NewRecorder()
		h.ServeHTTP(responseRecorder, r)

		logged := recorded.All()
		assert.Len(t, logged, 1)
		assert.Equal(t, "error parsing user sign-in cookie", logged[0].Entry.Message)

		responseRecorder.Flush()
		assert.Equal(t, http.StatusUnauthorized, responseRecorder.Code)
		assert.Equal(t, "Unauthorized\n", responseRecorder.Body.String())

		cookies := responseRecorder.Result().Cookies()
		assert.Len(t, cookies, 1)

		assert.Equal(t, "sign-in", cookies[0].Name)
		assert.Equal(t, "", cookies[0].Value)
		assert.Equal(t, -1, cookies[0].MaxAge)
	}

	{ // Invalid sign-in
		userService := mockUserService{
			"testUser|badToken": {
				signIn: nil,
				err:    user.ServiceBadAuth,
			},
		}

		userCookieAuth, err := user.NewCookieAuth("sign-in", securecookie.New(bytes.Repeat([]byte{'0'}, 64), bytes.Repeat([]byte{'0'}, 32)))
		assert.NoError(t, err)

		core, recorded := observer.New(zapcore.WarnLevel)
		logger := zap.New(core)

		gch := &getContextHandler{}
		cm, err := ContextMiddleware(userService, userCookieAuth, logger)
		assert.NoError(t, err)
		h := cm(gch)

		r := httptest.NewRequest("GET", "/test", nil)
		userCookie, err := userCookieAuth.Make("testUser", "badToken")
		assert.NoError(t, err)
		r.AddCookie(&http.Cookie{Name: "sign-in", Value: userCookie})
		responseRecorder := httptest.NewRecorder()
		h.ServeHTTP(responseRecorder, r)

		logged := recorded.All()
		assert.Len(t, logged, 1)
		assert.Equal(t, "invalid sign-in token", logged[0].Entry.Message)

		responseRecorder.Flush()
		assert.Equal(t, http.StatusUnauthorized, responseRecorder.Code)
		assert.Equal(t, "Unauthorized\n", responseRecorder.Body.String())

		cookies := responseRecorder.Result().Cookies()
		assert.Len(t, cookies, 1)

		assert.Equal(t, "sign-in", cookies[0].Name)
		assert.Equal(t, "", cookies[0].Value)
		assert.Equal(t, -1, cookies[0].MaxAge)
	}

	{ // Backend error
		userService := mockUserService{
			"testUser|breaksBackend": {
				signIn: nil,
				err:    user.ServiceBackendError,
			},
		}

		userCookieAuth, err := user.NewCookieAuth("sign-in", securecookie.New(bytes.Repeat([]byte{'0'}, 64), bytes.Repeat([]byte{'0'}, 32)))
		assert.NoError(t, err)

		core, recorded := observer.New(zapcore.WarnLevel)
		logger := zap.New(core)

		gch := &getContextHandler{}
		cm, err := ContextMiddleware(userService, userCookieAuth, logger)
		assert.NoError(t, err)
		h := cm(gch)

		r := httptest.NewRequest("GET", "/test", nil)
		userCookie, err := userCookieAuth.Make("testUser", "breaksBackend")
		assert.NoError(t, err)
		r.AddCookie(&http.Cookie{Name: "sign-in", Value: userCookie})
		responseRecorder := httptest.NewRecorder()
		h.ServeHTTP(responseRecorder, r)

		logged := recorded.All()
		assert.Len(t, logged, 1)
		assert.Equal(t, "failed to check sign-in token", logged[0].Entry.Message)

		responseRecorder.Flush()
		assert.Equal(t, http.StatusInternalServerError, responseRecorder.Code)
		assert.Equal(t, "Internal server error\n", responseRecorder.Body.String())

		// Do not clear cookie for backend errors
		assert.Empty(t, responseRecorder.Result().Cookies())
	}

	{ // nil arguments
		_, err := ContextMiddleware(mockUserService{}, &user.CookieAuth{}, nil)
		assert.EqualError(t, err, "nil argument(s)")
		_, err = ContextMiddleware(mockUserService{}, nil, &zap.Logger{})
		assert.EqualError(t, err, "nil argument(s)")
		_, err = ContextMiddleware(nil, &user.CookieAuth{}, &zap.Logger{})
		assert.EqualError(t, err, "nil argument(s)")
	}
}
