package middleware

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/chiungyu/webits/context"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"go.uber.org/zap/zaptest/observer"
)

type calledHandler struct {
	called bool
}

func (ch *calledHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ch.called = true
}

func TestSignInMiddleware(t *testing.T) {
	{ // Base usage
		core, recorded := observer.New(zapcore.ErrorLevel)
		logger := zap.New(core)

		ch := &calledHandler{}
		sm, err := SignInMiddleware(logger)
		assert.NoError(t, err)
		h := sm(ch)

		r := httptest.NewRequest("GET", "/test", nil)
		r = context.New("testRequestID", "testUser", "testToken", nil).Set(r)
		assert.NoError(t, err)
		h.ServeHTTP(httptest.NewRecorder(), r)

		assert.Equal(t, 0, recorded.Len()) // No error
		assert.True(t, ch.called)
	}

	{ // Missing context
		core, recorded := observer.New(zapcore.ErrorLevel)
		logger := zap.New(core)

		ch := &calledHandler{}
		sm, err := SignInMiddleware(logger)
		assert.NoError(t, err)
		h := sm(ch)

		r := httptest.NewRequest("GET", "/test", nil)
		assert.NoError(t, err)
		responseRecorder := httptest.NewRecorder()
		h.ServeHTTP(responseRecorder, r)

		logged := recorded.All()
		assert.Len(t, logged, 1)
		assert.Equal(t, "context not filled before sign-in middleware is called", logged[0].Entry.Message)

		responseRecorder.Flush()
		assert.Equal(t, http.StatusInternalServerError, responseRecorder.Code)
		assert.Equal(t, "Internal server error\n", responseRecorder.Body.String())

		assert.False(t, ch.called)
	}

	{ // Missing user ID
		core, recorded := observer.New(zapcore.ErrorLevel)
		logger := zap.New(core)

		ch := &calledHandler{}
		sm, err := SignInMiddleware(logger)
		assert.NoError(t, err)
		h := sm(ch)

		r := httptest.NewRequest("GET", "/test", nil)
		r = context.New("testRequestID", "", "testToken", nil).Set(r)
		assert.NoError(t, err)
		responseRecorder := httptest.NewRecorder()
		h.ServeHTTP(responseRecorder, r)

		assert.Equal(t, 0, recorded.Len()) // No error

		responseRecorder.Flush()
		assert.Equal(t, http.StatusUnauthorized, responseRecorder.Code)
		assert.Equal(t, "Unauthorized\n", responseRecorder.Body.String())

		assert.False(t, ch.called)
	}

	{ // nil logger
		_, err := SignInMiddleware(nil)
		assert.EqualError(t, err, "nil logger")
	}
}
