package server

import (
	"context"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"os/signal"
	"path"
	"strconv"
	"syscall"
	"time"

	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"github.com/jessevdk/go-flags"
	"go.uber.org/zap"
)

// Options for server
type Options struct {
	Host            string         `long:"host" description:"service host" default:"" env:"HOST"`
	Port            int            `long:"port" description:"service port, defaults to 8080" default:"8080" env:"PORT"`
	NoTLS           bool           `long:"no-tls" description:"serve http instead of https"`
	TLSCertificate  flags.Filename `long:"tls-certificate" description:"TLS certificate" env:"TLS_CERTIFICATE"`
	TLSKey          flags.Filename `long:"tls-key" description:"TLS private key" env:"TLS_PRIVATE_KEY"`
	GracefulTimeout time.Duration  `long:"graceful-timeout" description:"grace period for server shutdown" default:"15s"`

	APIBasePath string         `long:"api-base-path" description:"serve api endpoints under the URL path" default:"/api/v1"`
	APISpecFile flags.Filename `long:"api-spec-file" description:"api doc file" default:"openapi.yml"`
	APISpecPath string         `long:"api-spec-path" description:"serve API spec at the URL path" default:"/openapi.yml"`
	APIUIPath   string         `long:"api-ui-path" description:"serve API UI at the URL path" default:"/api/docs"`

	UIFileDir flags.Filename `long:"ui-dir" description:"serves UI files under the directory"`
}

func initLogger() (*zap.Logger, error) {
	logger, err := zap.NewProduction()
	if err != nil {
		return nil, err
	}
	zap.ReplaceGlobals(logger)
	zap.RedirectStdLog(logger)
	zap.L().Info("logger initialized")
	return logger, nil
}

func setupRoutes(router *gin.Engine, options *Options, addAPIRoutes func(*gin.RouterGroup)) error {
	// API endpoints
	addAPIRoutes(router.Group(options.APIBasePath))

	// API spec and swagger UI
	router.StaticFile(options.APISpecPath, string(options.APISpecFile))
	{
		data := []byte(fmt.Sprintf(swaggerUI, options.APISpecPath))
		router.GET(options.APIUIPath, func(c *gin.Context) {
			c.Data(http.StatusOK, "text/html", data)
		})
	}

	// UI files
	if len(options.UIFileDir) != 0 {
		dir := string(options.UIFileDir)
		files, err := ioutil.ReadDir(dir)
		if err != nil {
			return fmt.Errorf("failed to read UIFileDir: %w", err)
		}
		for _, f := range files {
			name := f.Name()
			filepath := path.Join(dir, name)
			if f.IsDir() {
				router.Static("/"+name, filepath)
			} else if name == "index.html" {
				router.StaticFile("/", filepath)
			} else {
				router.StaticFile("/"+name, filepath)
			}
		}
	}
	return nil
}

func listenAndServe(server *http.Server, options *Options, errChan chan<- error) {
	defer zap.L().Sync()
	var err error
	if options.NoTLS {
		zap.L().Info("serving with ListenAndServe", zap.String("addr", server.Addr))
		err = server.ListenAndServe()
	} else {
		zap.L().Info("serving with ListenAndServeTLS", zap.String("addr", server.Addr))
		err = server.ListenAndServeTLS(string(options.TLSCertificate), string(options.TLSKey))
	}
	if err != nil && err != http.ErrServerClosed {
		errChan <- fmt.Errorf("listenAndServe failed: %w", err)
	}
}

func serve(server *http.Server, options *Options, errChan chan<- error) {
	go listenAndServe(server, options, errChan)

	quit := make(chan os.Signal)
	// kill (no param) default sends syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be caught
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	signal := <-quit
	zap.L().Info("shutting down", zap.Stringer("signal", signal))

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), options.GracefulTimeout)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		errChan <- fmt.Errorf("graceful shutdown failed: %w", err)
		return
	}
	close(errChan)
}

// RunServer runs a new server with the provided options and API routes
func RunServer(options *Options, addAPIRoutes func(*gin.RouterGroup)) error {
	logger, err := initLogger()
	if err != nil {
		return err
	}

	router := gin.New()
	router.Use(
		ginzap.Ginzap(logger, time.RFC3339, true), // RFC3339 with UTC time
		ginzap.RecoveryWithZap(logger, true))
	if err := setupRoutes(router, options, addAPIRoutes); err != nil {
		return err
	}

	server := &http.Server{
		Addr:    net.JoinHostPort(options.Host, strconv.Itoa(options.Port)),
		Handler: router,
	}

	errChan := make(chan error, 1)
	go serve(server, options, errChan)
	return <-errChan
}

// Adapted from github.com/go-openapi/runtime/middleware/swaggerui.go
const swaggerUI = `
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
		<title>API</title>

    <link rel="stylesheet" type="text/css" href="https://unpkg.com/swagger-ui-dist/swagger-ui.css" >
    <link rel="icon" type="image/png" href="https://unpkg.com/swagger-ui-dist/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="https://unpkg.com/swagger-ui-dist/favicon-16x16.png" sizes="16x16" />
    <style>
      html
      {
        box-sizing: border-box;
        overflow: -moz-scrollbars-vertical;
        overflow-y: scroll;
      }

      *,
      *:before,
      *:after
      {
        box-sizing: inherit;
      }

      body
      {
        margin:0;
        background: #fafafa;
      }
    </style>
  </head>

  <body>
    <div id="swagger-ui"></div>

    <script src="https://unpkg.com/swagger-ui-dist/swagger-ui-bundle.js"> </script>
    <script src="https://unpkg.com/swagger-ui-dist/swagger-ui-standalone-preset.js"> </script>
    <script>
    window.onload = function() {
      // Begin Swagger UI call region
      const ui = SwaggerUIBundle({
        url: '%s',
        dom_id: '#swagger-ui',
        deepLinking: true,
        presets: [
          SwaggerUIBundle.presets.apis,
          SwaggerUIStandalonePreset
        ],
        plugins: [
          SwaggerUIBundle.plugins.DownloadUrl
        ],
        layout: "StandaloneLayout"
      })
      // End Swagger UI call region

      window.ui = ui
    }
  </script>
  </body>
</html>
`
