package context

import (
	"context"
	"net/http"
)

type key int

const k key = 0

// Context set by
type Context struct {
	requestID string
	userID    string
	userToken string
	userData  interface{}
}

// New returns a new Context if all required objects are there.
func New(requestID, userID, userToken string, userData interface{}) *Context {
	return &Context{requestID: requestID, userID: userID, userToken: userToken, userData: userData}
}

// Set returns request with the context set.
func (c *Context) Set(r *http.Request) *http.Request {
	return r.WithContext(context.WithValue(r.Context(), k, c))
}

// Get gets the context set.
func Get(r *http.Request) *Context {
	if ctx, ok := r.Context().Value(k).(*Context); ok {
		return ctx
	}
	return nil
}

// RequestID getter
func (c *Context) RequestID() string {
	return c.requestID
}

// UserID getter
func (c *Context) UserID() string {
	return c.userID
}

// UserToken getter
func (c *Context) UserToken() string {
	return c.userToken
}

// UserData getter
func (c *Context) UserData() interface{} {
	return c.userData
}
