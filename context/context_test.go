package context

import (
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	r := httptest.NewRequest("GET", "/test", nil)
	assert.Nil(t, Get(r))

	withContext := New("requestID", "user", "token", 42).Set(r)
	ctx := Get(withContext)
	assert.NotNil(t, ctx)
	assert.Equal(t, "requestID", ctx.RequestID())
	assert.Equal(t, "user", ctx.UserID())
	assert.Equal(t, "token", ctx.UserToken())
	assert.Equal(t, 42, ctx.UserData())

	var nilContext *Context = nil
	withNilContext := nilContext.Set(r)
	assert.Nil(t, Get(withNilContext))
}
